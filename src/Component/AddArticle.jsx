import { Form, Button, Container, Row, Col } from "react-bootstrap";
import React, { Component } from "react";
import axios from "axios";

export default class AddArticle extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      description: "",
      titleError: "",
      descriptionError: "",
    };
    this.onChange = this.onChange.bind(this);
  }

  onChange(e) {
    this.setState({
      [e.target.name]: e.target.value,
    });
    console.log(this.state.title);
  }

  handleSubmit = (e) => {
    let article = {
      TITLE: this.state.title,
      DESCRIPTION: this.state.description,
    };

    if (article.TITLE === "") {
      this.setState({
        titleError: "field can't be blank",
      });
    }
    if (article.DESCRIPTION === "") {
      this.setState({
        descriptionError: "field can't be blank",
      });
    } 
    if (article.TITLE !== "" && article.DESCRIPTION !==""){
      axios
        .post("http://110.74.194.124:15011/v1/api/articles", article)
        .then((res) => {
          alert(res.data.MESSAGE);
        });
      this.setState({
        title: "",
        description: "",
        titleError: "",
        descriptionError: ""
      });
    }
  };

  render() {
    return (
      <div style={{ width: "80%", margin: "auto" }}>
        <h2>Add Article</h2>
        <Container>
          <Row>
            <Col lg={8}>
              <Form>
                <Form.Group>
                  <Form.Label>Title</Form.Label>
                  <Form.Control
                    name="title"
                    type="text"
                    placeholder="Enter Title"
                    value={this.state.title}
                    onChange={this.onChange}
                  />
                  <h6 style={{color : "red"}}>{this.state.titleError}</h6>
                </Form.Group>

                <Form.Group>
                  <Form.Label>Description</Form.Label>
                  <Form.Control
                    name="description"
                    type="text"
                    placeholder="Description"
                    value={this.state.description}
                    onChange={this.onChange}
                  />
                  <h6 style={{color : "red"}}>{this.state.descriptionError}</h6>
                </Form.Group>
                {/* it doesnt work before cus of onSubmit and type not specified and no callback (ie: this.handleSubmit()) */}
                <Button
                  variant="primary"
                  type="button"
                  onClick={() => this.handleSubmit()}
                >
                  Add
                </Button>
              </Form>
            </Col>
            {/* <Col lg={4}>
              <h2>{this.state.title}</h2>
              <h2>{this.state.description}</h2>
              <input type="file" id="file-input" name="ImageStyle" />
            </Col> */}
          </Row>
        </Container>
      </div>
    );
  }
}
