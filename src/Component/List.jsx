import React, { Component } from "react";
import { Table, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import axios from "axios";

export default class List extends Component {
  convertToDate(unformatDate){
    // let date = unformatDate;
    // let year= date.subString(0,4);
    // let month = date.subString(4,6);
    // let day = date.subString(6, 8);
    // let createdDate = day + "-" + month + "-" + year;
    // let year = unformatDate % 1000000;
    // return createdDate;
  }
  handleDelete(id) {
    axios
      .delete(`http://110.74.194.124:15011/v1/api/articles/${id}`)
      .then((res) => alert(res.data.MESSAGE));
  }

  render() {
    return (
      <div style={{ width: "90%", margin: "auto" }}>
        <h1 style={{ textAlign: "center" }}>Article Management</h1>
        <Link to="/add">
          <Button style={{ textAlign: "center", marginLeft : "45%", marginBottom : "2%"}} variant="dark">
            Add Article
          </Button>
        </Link>

        <Table striped bordered hover>
          <thead>
            <tr>
              <th>#</th>
              <th style={{ width: "10em" }}>Title</th>
              <th style={{ width: "20em" }}>Description</th>
              <th>Created Date</th>
              <th>Image</th>
              <th style={{ width: "14em" }}>Action</th>
            </tr>
          </thead>
          <tbody>
            {this.props.data.map((article) => (
              <tr key={article.ID}>
                <td>{article.ID}</td>
                <td>{article.TITLE}</td>
                <td>{article.DESCRIPTION}</td>
                <td>{article.CREATED_DATE}</td>
                <td>
                  <img src={article.IMAGE} alt="pic"></img>
                </td>
                <td style={{ alignContent: "center" }}>
                  <Button as={Link} to={`/view/id=${article.ID}`} variant="primary">
                    View
                  </Button>&nbsp;
                  <Button as={Link} to={`/edit/id=${article.ID}`} variant="warning">
                    Edit
                  </Button>&nbsp;
                  <Button
                    onClick={() => this.handleDelete(article.ID)}
                    variant="danger"
                  >
                    Delete
                  </Button>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      </div>
    );
  }
}
